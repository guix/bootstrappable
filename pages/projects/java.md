title: From C++ to Java
---

The Java Development Kit (JDK) is written in Java, so you need a JDK
to build the JDK.  One way to cut this dependency cycle is to use
older tools that were written in a time when Java was not yet popular
enough to write compilers in Java.

In Guix the Java bootstrap begins with Jikes, a Java compiler written
in C++.  We use it to build a simple version of GNU Classpath, the
Java standard library.  We chose version 0.93 because it is the last
version that can be built with Jikes.  With Jikes and this version of
GNU Classpath we can build JamVM, a Java Virtual Machine.  We build
version 1.5.1 because it is the last version of JamVM that works with
a version of GNU classpath that does not require ECJ.  These three
packages make up the bootstrap JDK.

This is sufficient to build an older version of Ant, which is needed
to build an older version of ECJ, an incremental Java compiler, both
of which are written in Java.

ECJ is needed to build the latest release (0.99) and the development
version of GNU Classpath.  The development version of GNU Classpath
has much more support for Java 1.6 than the latest release, but we
need to build 0.99 first to get a working version of javah.  ECJ, the
development version of GNU Classpath, and the latest version of JamVM
make up the second stage JDK with which we can build the OpenJDK 6
with the Icedtea 1.x build framework.  We then build the more recent
JDKs Icedtea 2.x and Icedtea 3.x for OpenJDK 7 and 8, respectively.

Moving on to JDK 9 and 10 is left as an exercise for the reader.

This is the dependency graph of the bootstrap JDK as implemented in
[GNU Guix](https://gnu.org/software/guix):

![dependency graph of the bootstrap JDK in GNU Guix](/images/jdk-bootstrap.png)

Unfortunately, most of the software needed for the bootstrap has been
abandoned.  To ensure that the JDK can be built from sources without
the need for an existing installation of the OpenJDK we propose to
continue maintaining the links of this bootstrap chain.

[← back to list of projects](/projects.html)
