(post :title "Projects"
      (page
       (p [Here are a couple of ongoing projects in the bootstrappable builds community.
           If you're interested in working on any of these projects please ,(anchor "contact us" "/who.html").])

       (h2 [GNU+Linux from scratch])
       (p [Any GNU+Linux distribution is rooted in bootstrap binaries that are taken for granted.  This set of binaries can be considered a seed from which we grow the system.
           ,(anchor "Learn more about how we reduce the size of the binary seeds!" "/projects/mes.html")])

       (h2 [Maintaining GCC version 4.7])
       (p [The C and C++ compilers of the GNU Compiler Collection make up the foundation of many free software distributions.
           Current versions of GCC are written in C++, which means that a C++ compiler is needed to build it from source.
           GCC 4.7 was the last version of the collection that could be built with a plain C compiler, a much simpler task.])
       (p [We propose to collectively maintain a subset of GCC 4.7 to ensure that we can build the foundation of free software
       distributions starting with a simple C compiler (such as tinyCC, pcc, etc).])

       (h2 [From C++ to the world of Java])
       (p [The Java Development Kit (JDK) is written in Java, so you
           need a JDK to build a JDK.
           ,(anchor "Learn more about how we cut the cycle!" "/projects/java.html")])

       (h2 [Bootstrapping the rest of the Java ecosystem])
       (p [In the Java world there are a lot of tools from testing frameworks
       like JUnit to build tools like Maven and Gradle that have self dependencies.
       Bootstrapping these is an ongoing effort. To see the current status and
       work to be done have a look at ,(anchor "Bootstrapping Java Tools"
       "/projects/java-tools.html")])

       (h2 [Bootstrapping JVM languages])
       (p [The Java Virtual Machine runs several other languages besides Java,
       each with their own tools and ecosystem.  The most prominent ones
       are Groovy, Clojure, Scala and Kotlin.  Some of these are already
       bootstrapped, but this is an ongoing effort.  To see the current status
       and work to be done have a look at ,(anchor "Bootstrapping JVM languages"
       "/projects/jvm-languages.html")])

       (h2 [Bootstrapping GHC with Hugs])
       (p [The Glasgow Haskell Compiler (GHC) is the most popular Haskell compiler.
           It is written in Haskell and since the first public release requires GHC to build newer versions of the compiler.
           It might be possible to build a first GHC from source with the Hugs interpreter and an older version of GHC.
           ,(anchor "This blog post describes a first attempt"
                    "https://elephly.net/posts/2017-01-09-bootstrapping-haskell-part-1.html")
           of this project.])))
